import jsonpickle

class DadoDeTreinamento():

    def __init__(self,tag,patterns,responses,context_set,context_filter):
        self.responses = responses
        self.patterns = patterns
        self.tag = tag
        self.context_set = context_set
        self.context_filter = context_filter

class IntentDados():

    def __init__(self,listaDeDados):
        self.intents = listaDeDados

if __name__ == '__main__':

    intents = IntentDados(None)
    listaDados = []
    jsonpickle.set_preferred_backend('json')
    jsonpickle.set_encoder_options('json', ensure_ascii=False)

    with open("dadosTreinamento.json","r+") as f:
        while (True):
            typed = None
            typed2 = None
            typed3 = None
            typed4 = None
            patterns = []
            responses = []
            context_set = None
            context_filter = None

            tag = input("Digite a TAG \n")

            while (typed != "S"):

                typed = input("Digite 1 ou mais patterns (digite um de cada vez), digite S para adicionar responses \n")

                if (typed != "S"):
                    patterns.append(typed)

            while (typed2 != "S"):
                typed2 = input("Digite 1 ou mais responses (digite um de cada vez), digite S para finalizar\n")

                if (typed2 != "S"):
                    responses.append(typed2)


            context_set = input("Essa pergunta inicia um context? se sim defina o context_set, se não, digite S")
            if (context_set == "S"):
                context_set = ""

            context_filter = input("Essa pergunta precisa de um context_set para ser feita? se sim defina o context_filter, se não, digite S")
            if (context_filter == "S"):
                context_filter = ""

            dado = DadoDeTreinamento(tag, patterns, responses, context_set, context_filter)

            listaDados.append(dado)

            intents.intents = listaDados
            json = jsonpickle.encode(intents, unpicklable=False)

            print(json)
            data = f.read()
            f.seek(0)
            f.write(json)
            f.truncate()
